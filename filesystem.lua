-- filesystem.lua
-- The filesystem Premake Module.

local m = {}

m._VERSION = "0.0.0"

function m.find(var, ...)
    local PATH = os.getenv("PATH")
    local found = var and os.getenv(var)
    if found and (os.isfile(found) or os.isdir(found)) then
        found = path.normalize(found)
        if os.is("windows") then
            return string.format("%%%s%%", var)
        elseif os.is("linux") or os.is("macosx") then
            return string.format("$%s", var)
        else
            return found
        end
    else
        local args = {var, ...}
        for _, v in ipairs(args) do
            found = os.locate(v)
            if found and (os.isfile(found) or os.isdir(found)) then
                found = path.normalize(found)
                return path.getname(found)
            else
                local dir = os.pathsearch(v, PATH)
                if dir and os.isdir(dir) then
                    found = path.normalize(path.join(dir, v))
                    return v
                end
            end
        end
    end
    return nil
end

local dirstack = {}

function m.print_dirstack(...)
    for i,v in ipairs(dirstack) do
        print(... or "", i,v)
    end
end

function m.pushd(str)
    if #dirstack == 0 then
        table.insert(dirstack, os.getcwd())
    end

    if os.chdir(str) then
        table.insert(dirstack, os.getcwd())
        return true
    else
        return nil, "Failed to push new directory."
    end
end

function m.popd(n)
    local i = 0
    while #dirstack > 1 and i < (n or 1) do
        i = i + 1
        table.remove(dirstack)
        local top = dirstack[#dirstack]
        if not os.chdir(top) then
            return nil, "Failed to pop directory"
        end
    end
    return true
end

function m.files_exist(...)
    local result = false
    for _,v in ipairs{...} do
        result = not table.isempty(os.matchfiles(v))
        if not result then
            break
        end
    end
    return result
end

function m.folders_exist(...)
    local result = false
    for _,v in ipairs{...} do
        result = not table.isempty(os.matchdirs(v))
        if not result then
            break
        end
    end
    return result
end

return m
